#!/bin/bash

#assuming in source
cd ..
#now out of source.

sampleName=$1
MaxEvent=$2
if [ -z "$2" ]  #-z checks if it is a null argument
  then
	MaxEvent=-1 
fi
eos="/eos/user/z/zhelun"
current_folder=$PWD
rm $eos/$sampleName/myfile.root
echo $eos/$sampleName/myfile.root deleted

cd build 
make
cd ../run 
rm -rf run_$sampleName
mkdir run_$sampleName
cd run_$sampleName
athena MCanalysis/MCanalysisAlgJobOptions.py  --evtMax=$MaxEvent

cd ..
cd ../source



