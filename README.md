This project is to analyze MC samples.(DAOD)

To set up, do:
```
setupATLAS
mkdir analysisMC
cd analysisMC
mkdir build run source
cd source
git clone https://gitlab.cern.ch/zhelun/mcanalysis.git . #The [.] make sure its is in the current dir, not producing a new dir
cd ../build
asetup AthAnalysis,21.2.39,here
mv CMakeLists.txt ../source
cmake ../source
source $TestArea/*/setup.sh
make
```

When logging back in, do:
```
setupATLAS
cd build
asetup
source $TestArea/*/setup.sh
```
or do
```
cd source
. ./setup.sh
```


To run it, do:
```
cd build 
make
cd ../run 
rm -rf run_test
mkdir run_test
cd run_test
athena MCanalysis/MCanalysisAlgJobOptions.py  --evtMax=10
```
Or run it with bash file:
```
. ./run.sh WlvAA -1
```

To Make plots, make sure first change the dir dependence in mkHist.C create the plot_dir needed


Add a variable by the pytho script:
```
python3 addVariable.py --Type vec_double --Name leadingLep_pt
```



adding a new library:

1.add lib in CMakelist
2.add [#include ...] in MCanalysis.h
3.make in build

committing job:
```
git status
git add [modified files]
git commit -m "[VERSION_NAME]"
git push origin
```
