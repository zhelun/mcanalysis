
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../MCanalysisAlg.h"

DECLARE_ALGORITHM_FACTORY( MCanalysisAlg )

DECLARE_FACTORY_ENTRIES( MCanalysis ) 
{
  DECLARE_ALGORITHM( MCanalysisAlg );
}
