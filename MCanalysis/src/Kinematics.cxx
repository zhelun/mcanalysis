#include "Kinematics.h"
#include <iostream>
#include <random>


double mag(double x, double y){
	return sqrt(x*x+y*y);
}
double DeltaPhi(double x, double y){
	//make sure it is between 0 and 2pi
	double deltaPhi=fmod( TMath::TwoPi()+x-y , TMath::TwoPi() );	
	if (deltaPhi > M_PI) deltaPhi = TMath::TwoPi() - deltaPhi;	
	return deltaPhi;
}


double DeltaEta(double x,double y){
	auto diff=x-y;
	return diff;
}

double DeltaR(double phi1,double eta1,double phi2,double eta2){
	double dphi=DeltaPhi(phi1,phi2);
	double deta=DeltaEta(eta1,eta2);
	double dR=mag(dphi,deta);
	return dR;
}


