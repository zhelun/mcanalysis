#include "MCanalysisAlg.h"
#include <iostream>
#include <random>

double mag(double,double);
double DeltaPhi(double,double);
double DeltaEta(double,double);
double DeltaR(double,double,double,double);
