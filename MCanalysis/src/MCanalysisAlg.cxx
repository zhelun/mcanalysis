// MCanalysis includes
#include "MCanalysisAlg.h"
#include <iostream>
#include <random>
#include "Kinematics.h"
 

//#include "xAODEventInfo/EventInfo.h"




MCanalysisAlg::MCanalysisAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration

}


MCanalysisAlg::~MCanalysisAlg() {}


StatusCode MCanalysisAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  //HERE IS AN EXAMPLE
  //We will create a histogram and a ttree and register them to the histsvc
  //Remember to configure the histsvc stream in the joboptions
  //

  m_myTree= new TTree("myTree","myTree");
  CHECK( histSvc()->regTree("/MYSTREAM/myTree", m_myTree) ); //registers tree to output stream inside a sub-directory
  m_myTree->Branch("select",&m_select);
  m_myTree->Branch("nHardElectrons",&m_nHardElectrons);
  m_myTree->Branch("nHardMuons",&m_nHardMuons);
  m_myTree->Branch("nHardTaus",&m_nHardTaus); 
  m_myTree->Branch("invarLV",&m_invarLV);
  m_myTree->Branch("leadingLep_pt",&m_leadingLep_pt);
  m_myTree->Branch("closestY_pt",&m_closestY_pt);
  m_myTree->Branch("leadingLep_eta",&m_leadingLep_eta);
  m_myTree->Branch("closestY_eta",&m_closestY_eta);
  m_myTree->Branch("njets",&m_njets);
  m_myTree->Branch("tauFlag",&m_tauFlag);
  m_myTree->Branch("TruthMET",&m_TruthMET);
  m_myTree->Branch("Ht",&m_Ht);
  m_myTree->Branch("pT_laMET",&m_pT_laMET);
  m_myTree->Branch("pq",&m_pq);
//cxxInsertTree

  nevents=0;
  n_select=0;
  return StatusCode::SUCCESS;
}

StatusCode MCanalysisAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}

StatusCode MCanalysisAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed



  //
  //Your main analysis code goes here
  //If you will use this algorithm to perform event skimming, you
  //should ensure the setFilterPassed method is called
  //If never called, the algorithm is assumed to have 'passed' by default
  //


  //HERE IS AN EXAMPLE
  //const xAOD::EventInfo* ei = 0;
  //CHECK( evtStore()->retrieve( ei , "EventInfo" ) );
  //ATH_MSG_INFO("eventNumber=" << ei->eventNumber() );
  //m_myHist->Fill( ei->averageInteractionsPerCrossing() ); //fill mu into histogram
nevents++;
  if (nevents%1000 == 0) {
    ATH_MSG_INFO (" " << nevents << " events processed");
  }

  const xAOD::EventInfo* eventInfo = 0;
  CHECK( evtStore()->retrieve( eventInfo , "EventInfo" ) );

  const xAOD::TruthParticleContainer* TruthElectrons= 0;
  CHECK(evtStore()->retrieve( TruthElectrons, "TruthElectrons"));	

  const xAOD::TruthParticleContainer* TruthMuons= 0;
  CHECK(evtStore()->retrieve( TruthMuons, "TruthMuons"));

  const xAOD::TruthParticleContainer* TruthTaus= 0;
  CHECK(evtStore()->retrieve( TruthTaus, "TruthTaus"));

  const xAOD::TruthParticleContainer* TruthPhotons= 0;
  CHECK(evtStore()->retrieve( TruthPhotons, "TruthPhotons"));
  
  const xAOD::TruthParticleContainer* TruthNeutrinos= 0;
  CHECK(evtStore()->retrieve( TruthNeutrinos, "TruthNeutrinos"));

  const xAOD::JetContainer* TruthR10Jets = 0;
  CHECK(evtStore()->retrieve( TruthR10Jets, "AntiKt10TruthTrimmedPtFrac5SmallR20Jets"));

  const xAOD::TruthParticleContainer* TruthBottom= 0;
  CHECK(evtStore()->retrieve( TruthBottom, "TruthBottom"));

  const xAOD::MissingETContainer* TruthMET=0;
  CHECK(evtStore()->retrieve( TruthMET, "MET_Truth"));

  const xAOD::TruthParticleContainer* HardScatterParticles= 0;
  CHECK(evtStore()->retrieve( HardScatterParticles, "HardScatterParticles"));

  //initializing branches:
  m_select=false;
  m_nHardElectrons=0;
  m_nHardMuons=0;
  m_nHardTaus=0;
  m_invarLV={};
m_leadingLep_pt=0;
m_closestY_pt=0;
m_leadingLep_eta=0;
m_closestY_eta=0;
m_njets=0;
m_tauFlag=0;
m_TruthMET=0;
m_Ht=0;
m_pT_laMET=0;
m_pq=0;
//cxxInsertVariable

  
  ConstDataVector<xAOD::TruthParticleContainer> hardLeptons(SG::VIEW_ELEMENTS);
  double leadingLep_pt=0.0;
  double leadingLep_phi=0.0;
  double leadingLep_eta=0.0;

 for(const auto *el : *TruthElectrons) {
      double el_pt=el->pt();
      double el_phi=el->phi();
      double el_eta=el->eta();	
      if(el_pt>leadingLep_pt){
	 leadingLep_pt= el_pt;
	 leadingLep_phi=el_phi;
	 leadingLep_eta=el_eta;
	}
      //select hard ones
      if((el_pt > 25*1000)&&(std::abs(el->eta())<2.5)) {
	hardLeptons.push_back(el);
        m_nHardElectrons++;
		}
	}
  
  for(const auto *mu : *TruthMuons) {
      double mu_pt=mu->pt();
      double mu_phi=mu->phi();
      double mu_eta=mu->eta();
      if(mu_pt>leadingLep_pt){ 
         leadingLep_pt= mu_pt;
         leadingLep_phi=mu_phi;
         leadingLep_eta=mu_eta;
        }

      if((mu_pt > 25*1000)&&(std::abs(mu->eta())<2.5)) {
	hardLeptons.push_back(mu);
        m_nHardMuons++;
		}
	}

//tau is not included in the lepton selection
  //    for(const auto *tau : *TruthTaus) {
 //     double tau_pt=tau->pt();
 //     double tau_phi=tau->phi();
 //     double tau_eta=tau->eta();
//      if(tau_pt>leadingLep_pt){ 
//         leadingLep_pt=tau_pt;
//       leadingLep_phi=tau_phi;
//       leadingLep_eta=tau_eta;
//	 m_tauFlag=true;  
  //    }

     // if((tau_pt > 25*1000)&&(std::abs(tau->eta())<2.5)){
//	 hardLeptons.push_back(tau);
//    m_nHardTaus++;

//		}
//	}

  m_leadingLep_pt=leadingLep_pt;
  m_leadingLep_eta=leadingLep_eta;
  double closestY_pt=0.0;
  double closestY_eta=0.0;
  double dR_yl=10000.0;

  ConstDataVector<xAOD::TruthParticleContainer> hardPhotons(SG::VIEW_ELEMENTS);
  for(const auto *pho : *TruthPhotons) {
      double pho_phi=pho->phi();
      double pho_eta=pho->eta();
      double pho_pt=pho->pt();
      double cur_dR=DeltaR(leadingLep_phi,leadingLep_eta,pho_phi,pho_eta);

      if ((pho_pt > 25*1000)&&(std::abs(pho->eta())<2.5)) hardPhotons.push_back(pho);
      if(cur_dR<dR_yl){
	dR_yl=cur_dR;
        closestY_pt=pho_pt;
	closestY_eta=pho_eta;
		}	
        }
  m_closestY_pt=closestY_pt;
  m_closestY_eta=closestY_eta;
//hardJets
  ConstDataVector<xAOD::JetContainer> hardJets(SG::VIEW_ELEMENTS);
  for(const auto *jet : *TruthR10Jets) {
      if((jet)->pt() > 20*1000) hardJets.push_back(jet);
	}
  m_njets=hardJets.size();
//Non-interacting particle MET(neutrinos):
  double NonIntMET_px=0.0;
  double NonIntMET_py=0.0;
  double NonIntMET=0.0;
  for(const auto *met : *TruthMET){
	if(met->name()=="NonInt"){
		NonIntMET_px=met->mpx();
		NonIntMET_py=met->mpy();
		NonIntMET=met->met();
		}
	}
  m_TruthMET=NonIntMET;

  bool Select=false;
  //first, implement a 70% efficiency b selection
  int random;
  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_int_distribution<int> dist(1, 10);
  random=dist(mt);
  bool hasBottom=false;
  if ((TruthBottom->size()!=0)&&(random>=7)) hasBottom=true;
 
  bool s_hardLepton=hardLeptons.size()>=1;
  bool s_hardPhoton=hardPhotons.size()>=1;
  bool s_hardJets=hardJets.size()<=2;
  bool s_bottom=!(hasBottom);
  //combining:
  bool s_baseCut=s_hardLepton&&s_hardPhoton&&s_hardJets&&s_bottom;
  
  if(s_baseCut)Select=true;
  
  m_select=Select;
  if(Select) {
	n_select++;
	//Ht is the scalar sum of pT of visible(hardpho,hardlep,jets):
	double Ht=0.0;

	//find the leading lepton
	const xAOD::TruthParticle* leadingLep=0;
	double leadingLep_pt=0.0;
	for(const auto lep : hardLeptons){
		double lep_pt=lep->pt();
		Ht+=lep_pt;
		if(lep_pt>leadingLep_pt){
			leadingLep_pt=lep_pt;
			leadingLep=lep;
		}
	}

	//finding the photon that is closest:
	const xAOD::TruthParticle* photon=0;
	double min_dR=1000.0;
	for(const auto pho:hardPhotons){
		Ht+=pho->pt();
		double cur_dR=DeltaR(pho->phi(),pho->eta(),leadingLep->phi(),leadingLep->eta());
		if(cur_dR<min_dR){
			min_dR=cur_dR;
			photon=pho;
		}		
	}

	//finding closest neutrino:
	const xAOD::TruthParticle* neutrino=0;
        double min_dRneutrino=1000.0;
	for(const auto *neu : *TruthNeutrinos){
                double cur_dR=DeltaR(neu->phi(),neu->eta(),leadingLep->phi(),leadingLep->eta());
                if(cur_dR<min_dRneutrino){
                        min_dRneutrino=cur_dR;
                        neutrino=neu;
                }
        }

	//reconstruct W:	
	xAOD::IParticle::FourMom_t mtm_lepton=leadingLep->p4();
	xAOD::IParticle::FourMom_t mtm_neutrino=neutrino->p4();	
        float invarLV=(mtm_lepton+mtm_neutrino).M();
	m_invarLV.push_back(invarLV);
 		

	//Add Jets to Ht:
	for(const auto jet: hardJets){
		Ht+=jet->pt();
	}
 	m_Ht=Ht;	
	//Threept vector sum:
	double pT_laMETx=leadingLep->px()+photon->px()+NonIntMET_px;
	double pT_laMETy=leadingLep->py()+photon->py()+NonIntMET_py;
	double pT_laMET=mag(pT_laMETx,pT_laMETy);
	m_pT_laMET=pT_laMET;
	//dot product of four momenta of lep and phto
	xAOD::IParticle::FourMom_t mtm_pho=photon->p4();
	double pq=mtm_pho.Dot(mtm_lepton);
	m_pq=pq;
   }//if select

  // fill the branches for outpur TTree
  m_myTree->Fill();

  setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}

StatusCode MCanalysisAlg::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}
