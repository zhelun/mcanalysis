#ifndef MCANALYSIS_MCANALYSISALG_H
#define MCANALYSIS_MCANALYSISALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include <xAODCore/AuxContainerBase.h>
#include <xAODCore/ShallowCopy.h>
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include <xAODJet/JetContainer.h>
#include <xAODJet/JetAuxContainer.h>
#include <xAODMissingET/MissingETContainer.h>
#include <xAODMissingET/MissingETAuxContainer.h>

#include <iostream>
#include "AthContainers/ConstDataVector.h"
#include "AthContainers/DataVector.h"
#include <TSystem.h>
#include <math.h> 
#include <TFile.h>
#include <TMatrixD.h>
#include <sstream>
//Example ROOT Includes
//#include "TTree.h"
//#include "TH1D.h"



class MCanalysisAlg: public ::AthAnalysisAlgorithm { 
 public: 
  MCanalysisAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~MCanalysisAlg(); 

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed
  

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


 private: 

   //Example algorithm property, see constructor for declaration:
   //int m_nProperty = 0;

   //Example histogram, see initialize method for registration to output histSvc
   //TH1D* m_myHist = 0;
   TTree* m_myTree = 0;
	int nevents;	
	int n_select;

	bool m_select;
	int m_nHardElectrons;
	int m_nHardMuons;
	int m_nHardTaus;
	std::vector<double> m_invarLV;
double m_leadingLep_pt;
double m_closestY_pt;
double m_leadingLep_eta;
double m_closestY_eta;
int m_njets;
bool m_tauFlag;
double m_TruthMET;
double m_Ht;
double m_pT_laMET;
double m_pq;
//headerInsertVariable

}; 

#endif //> !MCANALYSIS_MCANALYSISALG_H
