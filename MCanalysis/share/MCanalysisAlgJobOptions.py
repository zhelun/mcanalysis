#Skeleton joboption for a simple analysis job

from os import listdir
from os.path import isfile, join


jps.AthenaCommonFlags.AccessMode = "ClassAccess"              #Choose from TreeAccess,BranchAccess,ClassAccess,AthenaAccess,POOLAccess
#jps.AthenaCommonFlags.TreeName = "MyTree"                    #when using TreeAccess, must specify the input tree name

SampleName="WlvAA"
outputPath="MYSTREAM:/eos/user/z/zhelun/"+SampleName
outputName="/myfile.root"
output=outputPath+outputName
jps.AthenaCommonFlags.HistOutputs = [output]  #register output files like this. MYSTREAM is used in the code

athAlgSeq += CfgMgr.MCanalysisAlg()                               #adds an instance of your alg to the main alg sequence


#---- Options you could specify on command line -----
#jps.AthenaCommonFlags.EvtMax=-1                          #set on command-line with: --evtMax=-1
#jps.AthenaCommonFlags.SkipEvents=0                       #set on command-line with: --skipEvents=0

files="/eos/user/z/zhelun/"+SampleName+"/"
files=files+"DAOD_TRUTH1."+SampleName+".pool.root"
#base_name="/eos/user/z/zhelun/data/mc16_13TeV.361204.Pythia8_A2_MSTW2008LO_SD_minbias.merge.AOD.e3639_s3245_r10169_r10046_tid13642307_00"
#files=[base_name+"/"+f for f in listdir(base_name) if isfile(join(base_name, f))]

#import AthenaPoolCnvSvc.ReadAthenaPool
#svcMgr.EventSelector.InputCollections = [files]
#jps.AthenaCommonFlags.FilesInput =svcMgr.EventSelector.InputCollections
jps.AthenaCommonFlags.FilesInput = [files]

include("AthAnalysisBaseComps/SuppressLogging.py")              #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above

