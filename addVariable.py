#!/usr/bin/python
import sys
import os
import argparse #require python3

#run it like:
#python3 addVariables.py -Type vec_double --Name myVec


#Input:
parser=argparse.ArgumentParser()
parser.add_argument("--Type","-t",help="set variable type")
parser.add_argument("--Name","-n",help="set variable name")
args=parser.parse_args()

if args.Type:
	v_type=args.Type

	if (v_type=="vec_double")or(v_type=="vec_int")or(v_type=="vec_float"):
      	  v_type="std::vector<"+v_type[4:]+">"
	print(v_type)

if args.Name:
	name=args.Name
	cxx_name="m_"+name

source_dir=os.getcwd()

src=source_dir+"/MCanalysis/src/"
cxx_file=src+"MCanalysisAlg.cxx"
header_file=src+"MCanalysisAlg.h"
macros_file=source_dir+"/MCanalysis/macros/mkHist.C"

#Marker comment:
header_marker="//headerInsertVariable"
header_line=v_type+" "+cxx_name+";"

cxx_tree_marker="//cxxInsertTree"
cxx_tree_line="  m_myTree->Branch(\""+name+"\",&"+cxx_name+");"

cxx_var_marker="//cxxInsertVariable"
if (v_type=="double")or(v_type=="int")or(v_type=="float")or(v_type=="bool"):
	cxx_var_line=cxx_name+"=0;"
elif (v_type=="std::vector<double>")or(v_type=="std::vector<int>")or(v_type=="std::vector<float>"):
	cxx_var_line=cxx_name+"={};"

macros_tree_marker="//macrosInsertTree"
macros_tree_line="tree->SetBranchAddress(\""+name+"\",&"+name+");"
macros_var_marker="//macarosInsertVariables"
if (v_type=="double")or(v_type=="int")or(v_type=="float")or(v_type=="bool"):
        macros_var_line=v_type+" "+name+"=0;"
elif (v_type=="std::vector<double>")or(v_type=="std::vector<int>")or(v_type=="std::vector<float>"):
        macros_var_line=v_type+"* "+name+"=0;"

#Declare variable in header:
with open(header_file,"r") as file:
    lines=file.readlines()
    for i in range(len(lines)):
       if header_marker in lines[i]:
           lines[i]=header_line+"\n"+header_marker+"\n"      
with open(header_file,"w") as file:
    file.writelines(lines)
print("Declared variable in header file:\n    ",header_line)


#modify cxx
with open(cxx_file,"r") as file:
    lines=file.readlines()
    for i in range(len(lines)):
       if cxx_tree_marker in lines[i]:
           lines[i]=cxx_tree_line+"\n"+cxx_tree_marker+"\n"
           print("adding tree in cxx file:\n    ",cxx_tree_line)
       if cxx_var_marker in lines[i]:
           lines[i]=cxx_var_line+"\n"+cxx_var_marker+"\n"
           print("Initializing variables in cxx file:\n    ",cxx_var_line)
with open(cxx_file,"w") as file:
    file.writelines(lines)

#modify macros
with open(macros_file,"r") as file:
    lines=file.readlines()
    for i in range(len(lines)):
       if macros_tree_marker in lines[i]:
           lines[i]=macros_tree_line+"\n"+macros_tree_marker+"\n"
           print("adding tree in macros file:\n    ",macros_tree_line)
       if macros_var_marker in lines[i]:
           lines[i]=macros_var_line+"\n"+macros_var_marker+"\n"
           print("Initializing variables in macros file:\n    ",macros_var_line)
with open(macros_file,"w") as file:
    file.writelines(lines)



